/**
 * @file
 * US State Map admin js
 */

(function ($) {
  
   Drupal.behaviors.base = {
    attach: function(context) {
        var f = $.farbtastic('#usstatemap-color-picker');
        var p = $('#usstatemap-color-picker').css('opacity', 0.25);
        var selected;
        $('.usstatemap-color')
        .each(function () { f.linkTo(this); $(this).css('opacity', 0.75); })
        .focus(function() {
            if (selected) {
              $(selected).css('opacity', 0.75).removeClass('usstatemap-color-selected');
            }
        f.linkTo(this);
        p.css('opacity', 1);
        $(selected = this).css('opacity', 1).addClass('usstatemap-color-selected');
        });
    }
   }
 
})(jQuery);
